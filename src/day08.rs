/*--- Day 8: Treetop Tree House ---

The expedition comes across a peculiar patch of tall trees all planted carefully in a grid. The Elves explain that a previous expedition planted these trees as a reforestation effort. Now, they're curious if this would be a good location for a tree house.

First, determine whether there is enough tree cover here to keep a tree house hidden. To do this, you need to count the number of trees that are visible from outside the grid when looking directly along a row or column.

The Elves have already launched a quadcopter to generate a map with the height of each tree (your puzzle input). For example:

30373
25512
65332
33549
35390

Each tree is represented as a single digit whose value is its height, where 0 is the shortest and 9 is the tallest.

A tree is visible if all of the other trees between it and an edge of the grid are shorter than it. Only consider trees in the same row or column; that is, only look up, down, left, or right from any given tree.

All of the trees around the edge of the grid are visible - since they are already on the edge, there are no trees to block the view. In this example, that only leaves the interior nine trees to consider:

    The top-left 5 is visible from the left and top. (It isn't visible from the right or bottom since other trees of height 5 are in the way.)
    The top-middle 5 is visible from the top and right.
    The top-right 1 is not visible from any direction; for it to be visible, there would need to only be trees of height 0 between it and an edge.
    The left-middle 5 is visible, but only from the right.
    The center 3 is not visible from any direction; for it to be visible, there would need to be only trees of at most height 2 between it and an edge.
    The right-middle 3 is visible from the right.
    In the bottom row, the middle 5 is visible, but the 3 and 4 are not.

With 16 trees visible on the edge and another 5 visible in the interior, a total of 21 trees are visible in this arrangement.

Consider your map; how many trees are visible from outside the grid?
*/

const INPUT: &str = include_str!("day08-input.txt");

struct Grid {
    data: Vec<u8>,
    column_count: usize,
    row_count: usize,
}

impl Grid {
    fn parse(input: &str) -> Self {
        let mut data = vec![];
        let mut column_count = 0;
        let mut row_count = 0;
        let mut lines = input.lines();

        while let Some(line) = lines.next() {
            column_count = line.len();
            row_count += 1;
            data.extend(line.chars().map(|c| c.to_digit(10).unwrap() as u8));
        }

        Grid {
            data,
            column_count,
            row_count,
        }
    }

    fn row(&self, index: usize) -> impl DoubleEndedIterator<Item = u8> + '_ {
        let start = index * self.column_count;
        let end = start + self.row_count;
        (&self.data[start..end]).into_iter().map(|&x| x)
    }

    fn col(&self, index: usize) -> impl DoubleEndedIterator<Item = u8> + '_ {
        let offset = index;
        let interval = self.column_count;
        self.data
            .as_slice()
            .into_iter()
            .map(|&x| x)
            .skip(offset)
            .step_by(interval)
    }

    fn trees(&self) -> impl Iterator<Item = Tree> + '_ {
        self.data.iter().enumerate().map(|(i, &height)| Tree {
            height,
            col: i % self.column_count,
            row: i / self.row_count,
        })
    }
}

struct Tree {
    height: u8,
    col: usize,
    row: usize,
}

impl Tree {
    fn above<'g>(&self, grid: &'g Grid) -> impl Iterator<Item = u8> + 'g {
        grid.col(self.col).rev().skip(grid.row_count - self.row)
    }

    fn below<'g>(&self, grid: &'g Grid) -> impl Iterator<Item = u8> + 'g {
        grid.col(self.col).skip(self.row + 1)
    }

    fn left<'g>(&self, grid: &'g Grid) -> impl Iterator<Item = u8> + 'g {
        grid.row(self.row).rev().skip(grid.column_count - self.col)
    }

    fn right<'g>(&self, grid: &'g Grid) -> impl Iterator<Item = u8> + 'g {
        grid.row(self.row).skip(self.col + 1)
    }

    fn is_visible(&self, grid: &Grid) -> bool {
        self.above(grid).all(|height| height < self.height)
            || self.below(grid).all(|height| height < self.height)
            || self.left(grid).all(|height| height < self.height)
            || self.right(grid).all(|height| height < self.height)
    }

    fn score(&self, grid: &Grid) -> usize {
        let above_score = self
            .above(grid)
            .position(|h| h >= self.height)
            .map(|pos| pos + 1)
            .unwrap_or(self.row);
        let below_score = self
            .below(grid)
            .position(|h| h >= self.height)
            .map(|pos| pos + 1)
            .unwrap_or(grid.row_count - self.row - 1);
        let left_score = self
            .left(grid)
            .position(|h| h >= self.height)
            .map(|pos| pos + 1)
            .unwrap_or(self.col);
        let right_score = self
            .right(grid)
            .position(|h| h >= self.height)
            .map(|pos| pos + 1)
            .unwrap_or(grid.column_count - self.col - 1);

        above_score * below_score * left_score * right_score
    }
}

pub fn part1() -> String {
    let grid = Grid::parse(INPUT);
    let count = grid.trees().filter(|t| t.is_visible(&grid)).count();
    format!("{count}")
}

/*--- Part Two ---

Content with the amount of tree cover available, the Elves just need to know the best spot to build their tree house: they would like to be able to see a lot of trees.

To measure the viewing distance from a given tree, look up, down, left, and right from that tree; stop if you reach an edge or at the first tree that is the same height or taller than the tree under consideration. (If a tree is right on the edge, at least one of its viewing distances will be zero.)

The Elves don't care about distant trees taller than those found by the rules above; the proposed tree house has large eaves to keep it dry, so they wouldn't be able to see higher than the tree house anyway.

In the example above, consider the middle 5 in the second row:

30373
25512
65332
33549
35390

    Looking up, its view is not blocked; it can see 1 tree (of height 3).
    Looking left, its view is blocked immediately; it can see only 1 tree (of height 5, right next to it).
    Looking right, its view is not blocked; it can see 2 trees.
    Looking down, its view is blocked eventually; it can see 2 trees (one of height 3, then the tree of height 5 that blocks its view).

A tree's scenic score is found by multiplying together its viewing distance in each of the four directions. For this tree, this is 4 (found by multiplying 1 * 1 * 2 * 2).

However, you can do even better: consider the tree of height 5 in the middle of the fourth row:

30373
25512
65332
33549
35390

    Looking up, its view is blocked at 2 trees (by another tree with a height of 5).
    Looking left, its view is not blocked; it can see 2 trees.
    Looking down, its view is also not blocked; it can see 1 tree.
    Looking right, its view is blocked at 2 trees (by a massive tree of height 9).

This tree's scenic score is 8 (2 * 2 * 1 * 2); this is the ideal spot for the tree house.

Consider each tree on your map. What is the highest scenic score possible for any tree?
*/

pub fn part2() -> String {
    let grid = Grid::parse(INPUT);
    let highest_score = grid.trees().map(|t| t.score(&grid)).max().unwrap();
    format!("{highest_score}")
}
