/*

--- Day 5: Supply Stacks ---

The expedition can depart as soon as the final supplies have been unloaded from the ships. Supplies are stored in stacks of marked crates, but because the needed supplies are buried under many other crates, the crates need to be rearranged.

The ship has a giant cargo crane capable of moving crates between stacks. To ensure none of the crates get crushed or fall over, the crane operator will rearrange them in a series of carefully-planned steps. After the crates are rearranged, the desired crates will be at the top of each stack.

The Elves don't want to interrupt the crane operator during this delicate procedure, but they forgot to ask her which crate will end up where, and they want to be ready to unload them as soon as possible so they can embark.

They do, however, have a drawing of the starting stacks of crates and the rearrangement procedure (your puzzle input). For example:

    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2

In this example, there are three stacks of crates. Stack 1 contains two crates: crate Z is on the bottom, and crate N is on top. Stack 2 contains three crates; from bottom to top, they are crates M, C, and D. Finally, stack 3 contains a single crate, P.

Then, the rearrangement procedure is given. In each step of the procedure, a quantity of crates is moved from one stack to a different stack. In the first step of the above rearrangement procedure, one crate is moved from stack 2 to stack 1, resulting in this configuration:

[D]
[N] [C]
[Z] [M] [P]
 1   2   3

In the second step, three crates are moved from stack 1 to stack 3. Crates are moved one at a time, so the first crate to be moved (D) ends up below the second and third crates:

        [Z]
        [N]
    [C] [D]
    [M] [P]
 1   2   3

Then, both crates are moved from stack 2 to stack 1. Again, because crates are moved one at a time, crate C ends up below crate M:

        [Z]
        [N]
[M]     [D]
[C]     [P]
 1   2   3

Finally, one crate is moved from stack 1 to stack 2:

        [Z]
        [N]
        [D]
[C] [M] [P]
 1   2   3

The Elves just need to know which crate will end up on top of each stack; in this example, the top crates are C in stack 1, M in stack 2, and Z in stack 3, so you should combine these together and give the Elves the message CMZ.

After the rearrangement procedure completes, what crate ends up on top of each stack?

*/

use regex::Regex;
use std::collections::VecDeque;

use crate::util::index_pair;

const INPUT: &str = include_str!("day05-input.txt");

#[derive(Debug)]
struct Grid {
    stacks: Vec<Stack>,
}

impl Grid {
    /// Returns parsed grid and remaining unused string
    fn parse(grid_str: &str) -> Self {
        let block_line_reg = Regex::new(
            r"(?x)
                (\[
                    (?P<val>[A-Z])
                \]\ ?)
                |
                (?P<empty>\ {3,4})
            ",
        )
        .unwrap();

        let mut stacks = vec![];

        for line in grid_str.lines() {
            let passed_all_blocks = line.starts_with(" ");

            if passed_all_blocks {
                break;
            }

            let mut stack_index = 0;

            for cap in block_line_reg.captures_iter(line) {
                if stacks.len() <= stack_index {
                    stacks.push(Stack(VecDeque::new()));
                }

                if let Some(val) = cap.name("val") {
                    let as_char = val.as_str().chars().next().unwrap();
                    stacks[stack_index].0.push_front(as_char);
                }

                stack_index += 1;
            }
        }

        Grid { stacks }
    }

    fn apply(&mut self, &Move { amount, from, to }: &Move) {
        let (from_stack, to_stack) = index_pair(&mut self.stacks, from - 1, to - 1);

        for _ in 0..amount {
            let val = from_stack.0.pop_back().unwrap();
            to_stack.0.push_back(val);
        }
    }

    fn apply2(&mut self, &Move { amount, from, to }: &Move) {
        let (from_stack, to_stack) = index_pair(&mut self.stacks, from - 1, to - 1);

        let from_len = from_stack.0.len();
        let mut to_move = from_stack.0.split_off(from_len - amount);
        to_stack.0.append(&mut to_move);
    }
}

#[derive(Debug)]
struct Stack(VecDeque<char>);

#[derive(Debug)]
struct Move {
    amount: usize,
    from: usize,
    to: usize,
}

impl Move {
    fn parse(line: &str) -> Self {
        match line.split_whitespace().collect::<Vec<_>>().as_slice() {
            &["move", amount, "from", from, "to", to] => Move {
                amount: amount.parse().unwrap(),
                from: from.parse().unwrap(),
                to: to.parse().unwrap(),
            },
            _ => panic!(),
        }
    }
}

fn doit(apply_move_fn: impl Fn(&mut Grid, &Move)) -> String {
    let (grid_str, moves_str) = INPUT.split_once("\n\n").unwrap();

    let mut grid = Grid::parse(grid_str);

    for line in moves_str.lines().filter(|l| !l.is_empty()) {
        let mv = Move::parse(line);

        apply_move_fn(&mut grid, &mv);
    }

    let top_vals = grid.stacks.iter().map(|Stack(s)| s.back().unwrap());
    let mut out = String::new();

    for &val in top_vals {
        out.push(val);
    }

    out
}

pub fn part1() -> String {
    doit(Grid::apply)
}

/*
--- Part Two ---

As you watch the crane operator expertly rearrange the crates, you notice the process isn't following your prediction.

Some mud was covering the writing on the side of the crane, and you quickly wipe it away. The crane isn't a CrateMover 9000 - it's a CrateMover 9001.

The CrateMover 9001 is notable for many new and exciting features: air conditioning, leather seats, an extra cup holder, and the ability to pick up and move multiple crates at once.

Again considering the example above, the crates begin in the same configuration:

    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

Moving a single crate from stack 2 to stack 1 behaves the same as before:

[D]
[N] [C]
[Z] [M] [P]
 1   2   3

However, the action of moving three crates from stack 1 to stack 3 means that those three moved crates stay in the same order, resulting in this new configuration:

        [D]
        [N]
    [C] [Z]
    [M] [P]
 1   2   3

Next, as both crates are moved from stack 2 to stack 1, they retain their order as well:

        [D]
        [N]
[C]     [Z]
[M]     [P]
 1   2   3

Finally, a single crate is still moved from stack 1 to stack 2, but now it's crate C that gets moved:

        [D]
        [N]
        [Z]
[M] [C] [P]
 1   2   3

In this example, the CrateMover 9001 has put the crates in a totally different order: MCD.

Before the rearrangement process finishes, update your simulation so that the Elves know where they should stand to be ready to unload the final supplies. After the rearrangement procedure completes, what crate ends up on top of each stack?

*/

pub fn part2() -> String {
    doit(Grid::apply2)
}
