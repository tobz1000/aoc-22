/*

--- Day 2: Rock Paper Scissors ---

The Elves begin to set up camp on the beach. To decide whose tent gets to be closest to the snack storage, a giant Rock Paper Scissors tournament is already in progress.

Rock Paper Scissors is a game between two players. Each game contains many rounds; in each round, the players each simultaneously choose one of Rock, Paper, or Scissors using a hand shape. Then, a winner for that round is selected: Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock. If both players choose the same shape, the round instead ends in a draw.

Appreciative of your help yesterday, one Elf gives you an encrypted strategy guide (your puzzle input) that they say will be sure to help you win. "The first column is what your opponent is going to play: A for Rock, B for Paper, and C for Scissors. The second column--" Suddenly, the Elf is called away to help with someone's tent.

The second column, you reason, must be what you should play in response: X for Rock, Y for Paper, and Z for Scissors. Winning every time would be suspicious, so the responses must have been carefully chosen.

The winner of the whole tournament is the player with the highest score. Your total score is the sum of your scores for each round. The score for a single round is the score for the shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won).

Since you can't be sure if the Elf is trying to help you or trick you, you should calculate the score you would get if you were to follow the strategy guide.

For example, suppose you were given the following strategy guide:

A Y
B X
C Z

This strategy guide predicts and recommends the following:

    In the first round, your opponent will choose Rock (A), and you should choose Paper (Y). This ends in a win for you with a score of 8 (2 because you chose Paper + 6 because you won).
    In the second round, your opponent will choose Paper (B), and you should choose Rock (X). This ends in a loss for you with a score of 1 (1 + 0).
    The third round is a draw with both players choosing Scissors, giving you a score of 3 + 3 = 6.

In this example, if you were to follow the strategy guide, you would get a total score of 15 (8 + 1 + 6).

What would your total score be if everything goes exactly according to your strategy guide?

*/

const INPUT: &str = include_str!("day02-input.txt");

#[derive(Debug, Clone, Copy)]
enum Hand {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

#[derive(Debug, Clone, Copy)]
enum Outcome {
    Win = 6,
    Draw = 3,
    Lose = 0,
}

#[derive(Debug, Clone, Copy)]
struct Game {
    opp: Hand,
    me: Hand,
}

impl Game {
    fn outcome(&self) -> Outcome {
        use Hand::*;
        use Outcome::*;

        match (self.opp, self.me) {
            (Rock, Paper) => Win,
            (Rock, Scissors) => Lose,
            (Paper, Rock) => Lose,
            (Paper, Scissors) => Win,
            (Scissors, Rock) => Win,
            (Scissors, Paper) => Lose,
            _ => Draw,
        }
    }

    fn score(&self) -> usize {
        self.me as usize + self.outcome() as usize
    }
}

pub fn part1() -> String {
    let games = INPUT.split('\n').map(|line| {
        match line.split_whitespace().collect::<Vec<_>>().as_slice() {
            &[opp_str, me_str] => {
                let opp = match opp_str {
                    "A" => Hand::Rock,
                    "B" => Hand::Paper,
                    "C" => Hand::Scissors,
                    _ => panic!("invalid line: {}", line),
                };

                let me = match me_str {
                    "X" => Hand::Rock,
                    "Y" => Hand::Paper,
                    "Z" => Hand::Scissors,
                    _ => panic!("invalid line: {}", line),
                };

                Game { opp, me }
            }
            _ => panic!("invalid line: {}", line),
        }
    });

    let total_score: usize = games.map(|g| g.score()).sum();

    format!("{total_score}")
}

/*
--- Part Two ---

The Elf finishes helping with the tent and sneaks back over to you. "Anyway, the second column says how the round needs to end: X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win. Good luck!"

The total score is still calculated in the same way, but now you need to figure out what shape to choose so the round ends as indicated. The example above now goes like this:

    In the first round, your opponent will choose Rock (A), and you need the round to end in a draw (Y), so you also choose Rock. This gives you a score of 1 + 3 = 4.
    In the second round, your opponent will choose Paper (B), and you choose Rock so you lose (X) with a score of 1 + 0 = 1.
    In the third round, you will defeat your opponent's Scissors with Rock for a score of 1 + 6 = 7.

Now that you're correctly decrypting the ultra top secret strategy guide, you would get a total score of 12.

Following the Elf's instructions for the second column, what would your total score be if everything goes exactly according to your strategy guide?

*/

struct Game2 {
    opp: Hand,
    outcome: Outcome,
}

impl Game2 {
    fn my_hand(&self) -> Hand {
        use Hand::*;
        use Outcome::*;

        match (self.opp, self.outcome) {
            (opp, Draw) => opp,
            (Rock, Win) => Paper,
            (Rock, Lose) => Scissors,
            (Paper, Win) => Scissors,
            (Paper, Lose) => Rock,
            (Scissors, Win) => Rock,
            (Scissors, Lose) => Paper,
        }
    }

    fn score(&self) -> usize {
        self.outcome as usize + self.my_hand() as usize
    }
}

pub fn part2() -> String {
    let games = INPUT.split('\n').map(|line| {
        match line.split_whitespace().collect::<Vec<_>>().as_slice() {
            &[opp_str, outcome_str] => {
                let opp = match opp_str {
                    "A" => Hand::Rock,
                    "B" => Hand::Paper,
                    "C" => Hand::Scissors,
                    _ => panic!("invalid line: {}", line),
                };

                let outcome = match outcome_str {
                    "X" => Outcome::Lose,
                    "Y" => Outcome::Draw,
                    "Z" => Outcome::Win,
                    _ => panic!("invalid line: {}", line),
                };

                Game2 { opp, outcome }
            }
            _ => panic!("invalid line: {}", line),
        }
    });

    let total_score: usize = games.map(|g| g.score()).sum();

    format!("{total_score}")
}
